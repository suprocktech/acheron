# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'plotmain.ui'
##
## Created by: Qt User Interface Compiler version 6.6.3
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QHeaderView, QLabel,
    QMainWindow, QMenu, QMenuBar, QProgressBar,
    QSizePolicy, QSpacerItem, QStackedWidget, QTabWidget,
    QToolBar, QTreeWidget, QTreeWidgetItem, QVBoxLayout,
    QWidget)

class Ui_PlotMainWindow(object):
    def setupUi(self, PlotMainWindow):
        if not PlotMainWindow.objectName():
            PlotMainWindow.setObjectName(u"PlotMainWindow")
        PlotMainWindow.resize(1200, 800)
        self.actionRescanUSB = QAction(PlotMainWindow)
        self.actionRescanUSB.setObjectName(u"actionRescanUSB")
        self.actionExit = QAction(PlotMainWindow)
        self.actionExit.setObjectName(u"actionExit")
        self.actionPreferences = QAction(PlotMainWindow)
        self.actionPreferences.setObjectName(u"actionPreferences")
        self.actionAbout = QAction(PlotMainWindow)
        self.actionAbout.setObjectName(u"actionAbout")
        self.actionDisableStreaming = QAction(PlotMainWindow)
        self.actionDisableStreaming.setObjectName(u"actionDisableStreaming")
        self.actionDisableStreaming.setCheckable(True)
        self.actionDisableArchiving = QAction(PlotMainWindow)
        self.actionDisableArchiving.setObjectName(u"actionDisableArchiving")
        self.actionDisableArchiving.setCheckable(True)
        self.actionFindTCPDevices = QAction(PlotMainWindow)
        self.actionFindTCPDevices.setObjectName(u"actionFindTCPDevices")
        self.actionEnableRFPower = QAction(PlotMainWindow)
        self.actionEnableRFPower.setObjectName(u"actionEnableRFPower")
        self.actionDisableRFPower = QAction(PlotMainWindow)
        self.actionDisableRFPower.setObjectName(u"actionDisableRFPower")
        self.actionClosableTabs = QAction(PlotMainWindow)
        self.actionClosableTabs.setObjectName(u"actionClosableTabs")
        self.actionClosableTabs.setCheckable(True)
        self.actionUpdateLatestStable = QAction(PlotMainWindow)
        self.actionUpdateLatestStable.setObjectName(u"actionUpdateLatestStable")
        self.actionUpdateSpecificBranch = QAction(PlotMainWindow)
        self.actionUpdateSpecificBranch.setObjectName(u"actionUpdateSpecificBranch")
        self.actionUpdateSpecificCommit = QAction(PlotMainWindow)
        self.actionUpdateSpecificCommit.setObjectName(u"actionUpdateSpecificCommit")
        self.actionUpdateCurrentBranch = QAction(PlotMainWindow)
        self.actionUpdateCurrentBranch.setObjectName(u"actionUpdateCurrentBranch")
        self.actionDownloadFirmware = QAction(PlotMainWindow)
        self.actionDownloadFirmware.setObjectName(u"actionDownloadFirmware")
        self.actionMarkFiles = QAction(PlotMainWindow)
        self.actionMarkFiles.setObjectName(u"actionMarkFiles")
        self.actionMarkDirectory = QAction(PlotMainWindow)
        self.actionMarkDirectory.setObjectName(u"actionMarkDirectory")
        self.actionConnectTCPDevice = QAction(PlotMainWindow)
        self.actionConnectTCPDevice.setObjectName(u"actionConnectTCPDevice")
        self.actionChannelTable = QAction(PlotMainWindow)
        self.actionChannelTable.setObjectName(u"actionChannelTable")
        self.actionShowLogs = QAction(PlotMainWindow)
        self.actionShowLogs.setObjectName(u"actionShowLogs")
        self.actionShowConfig = QAction(PlotMainWindow)
        self.actionShowConfig.setObjectName(u"actionShowConfig")
        self.actionReloadSchedule = QAction(PlotMainWindow)
        self.actionReloadSchedule.setObjectName(u"actionReloadSchedule")
        self.centralwidget = QWidget(PlotMainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(2, 0, 0, 0)
        self.stackedWidget = QStackedWidget(self.centralwidget)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.tabWidget = QTabWidget()
        self.tabWidget.setObjectName(u"tabWidget")
        self.treeWidget = QTreeWidget()
        self.treeWidget.setObjectName(u"treeWidget")
        self.treeWidget.setExpandsOnDoubleClick(False)
        self.tabWidget.addTab(self.treeWidget, "")
        self.stackedWidget.addWidget(self.tabWidget)
        self.logoPage = QWidget()
        self.logoPage.setObjectName(u"logoPage")
        self.verticalLayout_3 = QVBoxLayout(self.logoPage)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer = QSpacerItem(0, 101, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer)

        self.logoLayout = QVBoxLayout()
        self.logoLayout.setObjectName(u"logoLayout")

        self.verticalLayout_3.addLayout(self.logoLayout)

        self.verticalSpacer_2 = QSpacerItem(0, 101, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)

        self.verticalLayout_3.setStretch(0, 1)
        self.verticalLayout_3.setStretch(1, 5)
        self.verticalLayout_3.setStretch(2, 1)
        self.stackedWidget.addWidget(self.logoPage)

        self.verticalLayout.addWidget(self.stackedWidget)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(3, -1, 3, 1)
        self.warningLabel = QLabel(self.centralwidget)
        self.warningLabel.setObjectName(u"warningLabel")
        self.warningLabel.setStyleSheet(u"QLabel { color : red ; font-weight : bold ; font-size : 18px ; }")

        self.horizontalLayout_2.addWidget(self.warningLabel)

        self.uploadRateLabel = QLabel(self.centralwidget)
        self.uploadRateLabel.setObjectName(u"uploadRateLabel")
        self.uploadRateLabel.setMinimumSize(QSize(75, 25))

        self.horizontalLayout_2.addWidget(self.uploadRateLabel)

        self.uploadProgress = QProgressBar(self.centralwidget)
        self.uploadProgress.setObjectName(u"uploadProgress")
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.uploadProgress.sizePolicy().hasHeightForWidth())
        self.uploadProgress.setSizePolicy(sizePolicy)
        self.uploadProgress.setMinimumSize(QSize(175, 25))
        self.uploadProgress.setMaximum(5957928)
        self.uploadProgress.setValue(2359296)
        self.uploadProgress.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_2.addWidget(self.uploadProgress)

        self.uploadNameLabel = QLabel(self.centralwidget)
        self.uploadNameLabel.setObjectName(u"uploadNameLabel")

        self.horizontalLayout_2.addWidget(self.uploadNameLabel)

        self.horizontalSpacer_3 = QSpacerItem(0, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_3)

        self.activeTriggerLabel = QLabel(self.centralwidget)
        self.activeTriggerLabel.setObjectName(u"activeTriggerLabel")

        self.horizontalLayout_2.addWidget(self.activeTriggerLabel)

        self.datetimeLabel = QLabel(self.centralwidget)
        self.datetimeLabel.setObjectName(u"datetimeLabel")
        sizePolicy1 = QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.datetimeLabel.sizePolicy().hasHeightForWidth())
        self.datetimeLabel.setSizePolicy(sizePolicy1)
        self.datetimeLabel.setMinimumSize(QSize(200, 0))
        self.datetimeLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_2.addWidget(self.datetimeLabel)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        PlotMainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(PlotMainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1200, 22))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuStreaming = QMenu(self.menubar)
        self.menuStreaming.setObjectName(u"menuStreaming")
        self.menuSettings = QMenu(self.menubar)
        self.menuSettings.setObjectName(u"menuSettings")
        self.menuHelp = QMenu(self.menubar)
        self.menuHelp.setObjectName(u"menuHelp")
        self.menuCheckForUpdates = QMenu(self.menuHelp)
        self.menuCheckForUpdates.setObjectName(u"menuCheckForUpdates")
        self.menuDevices = QMenu(self.menubar)
        self.menuDevices.setObjectName(u"menuDevices")
        self.menuTools = QMenu(self.menubar)
        self.menuTools.setObjectName(u"menuTools")
        self.menuMarkForUpload = QMenu(self.menuTools)
        self.menuMarkForUpload.setObjectName(u"menuMarkForUpload")
        PlotMainWindow.setMenuBar(self.menubar)
        self.toolBar = QToolBar(PlotMainWindow)
        self.toolBar.setObjectName(u"toolBar")
        sizePolicy2 = QSizePolicy(QSizePolicy.Policy.MinimumExpanding, QSizePolicy.Policy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.toolBar.sizePolicy().hasHeightForWidth())
        self.toolBar.setSizePolicy(sizePolicy2)
        self.toolBar.setMovable(False)
        self.toolBar.setIconSize(QSize(24, 24))
        self.toolBar.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.toolBar.setFloatable(False)
        PlotMainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuDevices.menuAction())
        self.menubar.addAction(self.menuStreaming.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())
        self.menubar.addAction(self.menuTools.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.menuFile.addAction(self.actionExit)
        self.menuStreaming.addAction(self.actionDisableStreaming)
        self.menuStreaming.addAction(self.actionDisableArchiving)
        self.menuSettings.addAction(self.actionClosableTabs)
        self.menuSettings.addAction(self.actionReloadSchedule)
        self.menuSettings.addAction(self.actionPreferences)
        self.menuHelp.addAction(self.menuCheckForUpdates.menuAction())
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.actionChannelTable)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.actionShowLogs)
        self.menuHelp.addAction(self.actionShowConfig)
        self.menuHelp.addSeparator()
        self.menuHelp.addAction(self.actionAbout)
        self.menuCheckForUpdates.addAction(self.actionUpdateLatestStable)
        self.menuCheckForUpdates.addAction(self.actionUpdateCurrentBranch)
        self.menuCheckForUpdates.addAction(self.actionUpdateSpecificBranch)
        self.menuCheckForUpdates.addAction(self.actionUpdateSpecificCommit)
        self.menuDevices.addAction(self.actionRescanUSB)
        self.menuDevices.addAction(self.actionFindTCPDevices)
        self.menuDevices.addAction(self.actionConnectTCPDevice)
        self.menuTools.addAction(self.actionDownloadFirmware)
        self.menuTools.addSeparator()
        self.menuTools.addAction(self.menuMarkForUpload.menuAction())
        self.menuMarkForUpload.addAction(self.actionMarkFiles)
        self.menuMarkForUpload.addAction(self.actionMarkDirectory)
        self.toolBar.addAction(self.actionEnableRFPower)
        self.toolBar.addAction(self.actionDisableRFPower)
        self.toolBar.addAction(self.actionRescanUSB)
        self.toolBar.addAction(self.actionFindTCPDevices)

        self.retranslateUi(PlotMainWindow)
        self.actionExit.triggered.connect(PlotMainWindow.close)

        self.stackedWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(PlotMainWindow)
    # setupUi

    def retranslateUi(self, PlotMainWindow):
        PlotMainWindow.setWindowTitle(QCoreApplication.translate("PlotMainWindow", u"Acheron", None))
        self.actionRescanUSB.setText(QCoreApplication.translate("PlotMainWindow", u"Rescan USB", None))
        self.actionExit.setText(QCoreApplication.translate("PlotMainWindow", u"Exit", None))
        self.actionPreferences.setText(QCoreApplication.translate("PlotMainWindow", u"Preferences...", None))
        self.actionAbout.setText(QCoreApplication.translate("PlotMainWindow", u"About...", None))
        self.actionDisableStreaming.setText(QCoreApplication.translate("PlotMainWindow", u"Disable Streaming", None))
        self.actionDisableArchiving.setText(QCoreApplication.translate("PlotMainWindow", u"Disable Archiving", None))
        self.actionFindTCPDevices.setText(QCoreApplication.translate("PlotMainWindow", u"Find TCP Devices...", None))
        self.actionEnableRFPower.setText(QCoreApplication.translate("PlotMainWindow", u"Enable RF Power (0)", None))
        self.actionDisableRFPower.setText(QCoreApplication.translate("PlotMainWindow", u"Disable RF Power (0)", None))
        self.actionClosableTabs.setText(QCoreApplication.translate("PlotMainWindow", u"Easy Close Tabs", None))
        self.actionUpdateLatestStable.setText(QCoreApplication.translate("PlotMainWindow", u"Latest Stable", None))
        self.actionUpdateSpecificBranch.setText(QCoreApplication.translate("PlotMainWindow", u"Specific Branch...", None))
        self.actionUpdateSpecificCommit.setText(QCoreApplication.translate("PlotMainWindow", u"Specific Commit..", None))
        self.actionUpdateCurrentBranch.setText(QCoreApplication.translate("PlotMainWindow", u"Latest branch", None))
        self.actionDownloadFirmware.setText(QCoreApplication.translate("PlotMainWindow", u"Download Firmware...", None))
        self.actionMarkFiles.setText(QCoreApplication.translate("PlotMainWindow", u"Mark Files...", None))
        self.actionMarkDirectory.setText(QCoreApplication.translate("PlotMainWindow", u"Mark Directory...", None))
        self.actionConnectTCPDevice.setText(QCoreApplication.translate("PlotMainWindow", u"Manually Connect TCP Device...", None))
        self.actionChannelTable.setText(QCoreApplication.translate("PlotMainWindow", u"Channel Table...", None))
        self.actionShowLogs.setText(QCoreApplication.translate("PlotMainWindow", u"Show Logs on Disk...", None))
#if QT_CONFIG(tooltip)
        self.actionShowLogs.setToolTip(QCoreApplication.translate("PlotMainWindow", u"Show Logs on Disk", None))
#endif // QT_CONFIG(tooltip)
        self.actionShowConfig.setText(QCoreApplication.translate("PlotMainWindow", u"Show Config on Disk...", None))
        self.actionReloadSchedule.setText(QCoreApplication.translate("PlotMainWindow", u"Reload Schedule From Disk", None))
        ___qtreewidgetitem = self.treeWidget.headerItem()
        ___qtreewidgetitem.setText(8, QCoreApplication.translate("PlotMainWindow", u"Inactive Triggers", None));
        ___qtreewidgetitem.setText(7, QCoreApplication.translate("PlotMainWindow", u"Active Triggers", None));
        ___qtreewidgetitem.setText(6, QCoreApplication.translate("PlotMainWindow", u"Schedule Items", None));
        ___qtreewidgetitem.setText(5, QCoreApplication.translate("PlotMainWindow", u"Build Date", None));
        ___qtreewidgetitem.setText(4, QCoreApplication.translate("PlotMainWindow", u"Build Info", None));
        ___qtreewidgetitem.setText(3, QCoreApplication.translate("PlotMainWindow", u"Board Info", None));
        ___qtreewidgetitem.setText(2, QCoreApplication.translate("PlotMainWindow", u"Tag 2", None));
        ___qtreewidgetitem.setText(1, QCoreApplication.translate("PlotMainWindow", u"Tag 1", None));
        ___qtreewidgetitem.setText(0, QCoreApplication.translate("PlotMainWindow", u"Serial Number", None));
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.treeWidget), QCoreApplication.translate("PlotMainWindow", u"Devices", None))
        self.warningLabel.setText(QCoreApplication.translate("PlotMainWindow", u"Warning: Archiving Disabled", None))
        self.uploadRateLabel.setText(QCoreApplication.translate("PlotMainWindow", u"350 kB/s", None))
        self.uploadProgress.setFormat(QCoreApplication.translate("PlotMainWindow", u"%v/%m (%p%)", None))
        self.uploadNameLabel.setText(QCoreApplication.translate("PlotMainWindow", u"20181029T2241Z_HSS2143.apd", None))
        self.activeTriggerLabel.setText(QCoreApplication.translate("PlotMainWindow", u"Active Triggers: test", None))
        self.datetimeLabel.setText(QCoreApplication.translate("PlotMainWindow", u"0000-00-00T00:00:00Z", None))
        self.menuFile.setTitle(QCoreApplication.translate("PlotMainWindow", u"File", None))
        self.menuStreaming.setTitle(QCoreApplication.translate("PlotMainWindow", u"Streaming", None))
        self.menuSettings.setTitle(QCoreApplication.translate("PlotMainWindow", u"Settings", None))
        self.menuHelp.setTitle(QCoreApplication.translate("PlotMainWindow", u"Help", None))
        self.menuCheckForUpdates.setTitle(QCoreApplication.translate("PlotMainWindow", u"Check for Updates", None))
        self.menuDevices.setTitle(QCoreApplication.translate("PlotMainWindow", u"Devices", None))
        self.menuTools.setTitle(QCoreApplication.translate("PlotMainWindow", u"Tools", None))
        self.menuMarkForUpload.setTitle(QCoreApplication.translate("PlotMainWindow", u"Mark for Upload", None))
    # retranslateUi

