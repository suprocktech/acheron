# Acheron

## About
Acheron is a plotting application and data recorder for USB and TCP devices communicating via the Asphodel protocol.

The Asphodel communication protocol was developed by Suprock Technologies (http://www.suprocktech.com)

## License
Acheron is licensed under the ISC license.

The ISC license is a streamlined version of the BSD license, and permits usage in both open source and propretary projects.

## Pronunciation
Commonly pronounced as "ACK-er-on" in English. The word comes from Greek mythology, where Acheron is one of the five rivers in the underworld.
